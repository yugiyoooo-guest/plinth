#!/usr/bin/python3
#
# This file is part of FreedomBox.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
"""
Configuration helper for Matrix-Synapse server.
"""

import argparse
import filecmp
import os
import shutil
import sys

import yaml

from plinth import action_utils
from plinth.modules import letsencrypt
from plinth.modules.matrixsynapse import (CONFIG_FILE_PATH,
                                          get_configured_domain_name)


def parse_arguments():
    """Return parsed command line arguments as dictionary"""
    parser = argparse.ArgumentParser()
    subparsers = parser.add_subparsers(dest='subcommand', help='Sub command')

    subparsers.add_parser('post-install', help='Perform post install steps')
    subparsers.add_parser('enable', help='Enable matrix-synapse service')
    subparsers.add_parser('disable', help='Disable matrix-synapse service')
    help_pubreg = 'Enable/Disable/Status public user registration.'
    pubreg = subparsers.add_parser('public-registration', help=help_pubreg)
    pubreg.add_argument('command', choices=('enable', 'disable', 'status'),
                        help=help_pubreg)
    setup = subparsers.add_parser('setup', help='Set domain name for Matrix')
    setup.add_argument(
        '--domain-name',
        help='The domain name that will be used by Matrix Synapse')

    help_le = "Add/drop Let's Encrypt certificate if configured domain matches"
    subparser = subparsers.add_parser('letsencrypt', help=help_le)
    subparser.add_argument('command', choices=('add', 'drop', 'get-status'),
                           help='Whether to add or drop the certificate')
    subparser.add_argument('--domain',
                           help='Domain name to renew certificates for')

    subparsers.required = True
    return parser.parse_args()


def _get_certificate_status():
    """Return if the current certificate is an up-to-date LE certificate."""
    configured_domain = get_configured_domain_name()
    if not configured_domain:
        return False

    if not os.path.exists(letsencrypt.LIVE_DIRECTORY):
        return False

    source_dir = os.path.join(letsencrypt.LIVE_DIRECTORY, configured_domain)
    source_certificate_path = os.path.join(source_dir, 'fullchain.pem')
    source_private_key_path = os.path.join(source_dir, 'privkey.pem')

    dest_dir = '/etc/matrix-synapse'
    dest_certificate_path = os.path.join(dest_dir, 'homeserver.tls.crt')
    dest_private_key_path = os.path.join(dest_dir, 'homeserver.tls.key')

    if filecmp.cmp(source_certificate_path, dest_certificate_path) and \
       filecmp.cmp(source_private_key_path, dest_private_key_path):
        return True

    return False


def _update_tls_certificate():
    """Update the TLS certificate and private key used by Matrix Synapse.

    A valid certificate is necessary for federation with other instances
    starting with version 1.0.

    """
    configured_domain = get_configured_domain_name()
    if os.path.exists(letsencrypt.LIVE_DIRECTORY) and configured_domain:
        # Copy the latest Let's Encrypt certs into Synapse's directory.
        src_dir = os.path.join(letsencrypt.LIVE_DIRECTORY, configured_domain)
        source_certificate_path = os.path.join(src_dir, 'fullchain.pem')
        source_private_key_path = os.path.join(src_dir, 'privkey.pem')
    else:
        # Copy Apache's snake-oil certificate into Synapse's config directory.
        # The self-signed certificate doesn't really work (other Matrix
        # Synapse instances do not accept it). It is merely to prevent the
        # server from failing to startup because the files are missing.
        source_certificate_path = '/etc/ssl/certs/ssl-cert-snakeoil.pem'
        source_private_key_path = '/etc/ssl/private/ssl-cert-snakeoil.key'

    dest_dir = '/etc/matrix-synapse'
    dest_certificate_path = os.path.join(dest_dir, 'homeserver.tls.crt')
    dest_private_key_path = os.path.join(dest_dir, 'homeserver.tls.key')

    # Private key is only accessible to the user "matrix-synapse"
    # Group access is prohibited since it is "nogroup"
    old_mask = os.umask(0o133)
    shutil.copyfile(source_certificate_path, dest_certificate_path)
    os.umask(0o177)
    shutil.copyfile(source_private_key_path, dest_private_key_path)
    os.umask(old_mask)

    shutil.chown(dest_certificate_path, user='matrix-synapse', group='nogroup')
    shutil.chown(dest_private_key_path, user='matrix-synapse', group='nogroup')


def subcommand_post_install(_):
    """Perform post installation configuration."""
    with open(CONFIG_FILE_PATH) as config_file:
        config = yaml.load(config_file)

    config['max_upload_size'] = '100M'

    for listener in config['listeners']:
        if listener['port'] == 8448:
            listener['bind_addresses'] = ['::', '0.0.0.0']
            listener.pop('bind_address', None)

    # Setup ldap parameters
    config['password_providers'] = [{}]
    config['password_providers'][0][
        'module'] = 'ldap_auth_provider.LdapAuthProvider'
    ldap_config = {
        'enabled': True,
        'uri': 'ldap://localhost:389',
        'start_tls': False,
        'base': 'ou=users,dc=thisbox',
        'attributes': {
            'uid': 'uid',
            'name': 'uid',
            'mail': ''
        }
    }
    config['password_providers'][0]['config'] = ldap_config

    with open(CONFIG_FILE_PATH, 'w') as config_file:
        yaml.dump(config, config_file)

    _update_tls_certificate()

    if action_utils.service_is_running('matrix-synapse'):
        action_utils.service_restart('matrix-synapse')


def subcommand_setup(arguments):
    """Configure the domain name for matrix-synapse package."""
    domain_name = arguments.domain_name
    action_utils.dpkg_reconfigure('matrix-synapse',
                                  {'server-name': domain_name})
    _update_tls_certificate()
    subcommand_enable(arguments)


def subcommand_enable(_):
    """Enable service."""
    action_utils.service_enable('matrix-synapse')
    action_utils.webserver_enable('matrix-synapse-plinth')


def subcommand_disable(_):
    """Disable service."""
    action_utils.webserver_disable('matrix-synapse-plinth')
    action_utils.service_disable('matrix-synapse')


def subcommand_public_registration(argument):
    """Enable/Disable/Status public user registration."""
    with open(CONFIG_FILE_PATH) as config_file:
        config = yaml.load(config_file)

    if argument.command == 'status':
        if config['enable_registration']:
            print('enabled')
            return
        else:
            print('disabled')
            return
    elif argument.command == 'enable':
        config['enable_registration'] = True
    elif argument.command == 'disable':
        config['enable_registration'] = False

    with open(CONFIG_FILE_PATH, 'w') as config_file:
        yaml.dump(config, config_file)

    if action_utils.service_is_running('matrix-synapse'):
        action_utils.service_restart('matrix-synapse')


def subcommand_letsencrypt(arguments):
    """Add/drop usage of Let's Encrypt cert or show status.

    The command 'add' applies only to current domain, will be called by action
    'letsencrypt run_renew_hooks', when certbot renews the cert (if
    matrix-synapse is selected for cert use). Drop of a cert must be possible
    for any domain to respond to domain change.

    """
    if arguments.command == 'drop':
        print('Dropping certificates is not supported for Matrix Synapse.')
        return

    if arguments.command == 'get-status':
        print('valid' if _get_certificate_status() else 'invalid')
        return

    configured_domain = get_configured_domain_name()
    if arguments.domain is not None and \
       arguments.domain != configured_domain:
        print('Aborted: Current domain "{}" is not configured.'.format(
            arguments.domain))
        sys.exit(1)

    le_folder = os.path.join(letsencrypt.LIVE_DIRECTORY, configured_domain)
    if not os.path.exists(le_folder):
        print('Aborted: No certificate directory at %s.' % le_folder)
        sys.exit(2)

    _update_tls_certificate()

    action_utils.service_try_restart('matrix-synapse')


def main():
    arguments = parse_arguments()
    sub_command = arguments.subcommand.replace('-', '_')
    sub_command_method = globals()['subcommand_' + sub_command]
    sub_command_method(arguments)


if __name__ == '__main__':
    main()
